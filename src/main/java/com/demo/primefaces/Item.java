package com.demo.primefaces;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Data
@Entity
@Table(name = "items")
public class Item implements Serializable {

  @Id
  private String serialNum;
  private String title;
  private String itemDescription;
  private Date produceDate;
}
