package com.demo.primefaces;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

@Data
@Entity
@Table(name = "order_items")
@IdClass(OrderItemId.class)
public class OrderItem implements Serializable {

  @Id
  private Integer id;
  @ManyToOne
  @JoinColumn(name = "serialNum")
  private Item item;
  private int amount;
  @Id
  private Long orderId;
}
