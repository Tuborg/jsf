package com.demo.primefaces;

import com.demo.DateClient;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

@Named
@ViewScoped
public class OrdersView implements Serializable {

    private static final long serialVersionUID = 1L;

    @Autowired
    private OrderRepository orderRepository;
    @Autowired
    DateClient dateClient;

    private List<Order> orders;

    @PostConstruct
    public void init() {
        orders = orderRepository.findAll();
    }

    public List<Order> getOrders() {
        return orders;
    }

    public String getCurrentDate() {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        return format.format(dateClient.getDate().getDate().toGregorianCalendar().getTime());
    }
}
