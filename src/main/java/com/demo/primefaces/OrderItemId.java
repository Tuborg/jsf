package com.demo.primefaces;

import lombok.Data;

import java.io.Serializable;

@Data
public class OrderItemId implements Serializable{
  private Integer id;
  private Long orderId;
}
