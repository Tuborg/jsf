package com.demo.primefaces;

import lombok.Data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.*;

@Data
@Entity
@Table(name = "orders")
public class Order implements Serializable {

  private static final long serialVersionUID = 1L;

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;
  private String name;
  private String address;
  private Double amount;
  private Date orderDate;

  @OneToMany(
          mappedBy = "orderId",
          cascade = CascadeType.ALL,
          orphanRemoval = true
  )
  List<OrderItem> orderItems = new ArrayList<>();



}
