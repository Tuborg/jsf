package com.demo.primefaces;

import lombok.Getter;
import org.jibx.runtime.BindingDirectory;
import org.jibx.runtime.IBindingFactory;
import org.jibx.runtime.IUnmarshallingContext;
import org.jibx.runtime.JiBXException;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.UploadedFile;
import org.springframework.beans.factory.annotation.Autowired;

import javax.faces.view.ViewScoped;
import javax.inject.Named;
import java.io.*;

@Named
@ViewScoped
public class FileBean implements Serializable {
    @Autowired
    ItemRepository itemRepository;

    public void handleFileUpload(FileUploadEvent event) throws IOException {
        UploadedFile file = event.getFile();
        InputStream is = file.getInputstream();
        try {
            IBindingFactory bfact;
            bfact = BindingDirectory.getFactory(Item.class);
            IUnmarshallingContext uctx = bfact.createUnmarshallingContext();
            Items items = (Items) uctx.unmarshalDocument(is, null);
            items.itemsList.stream().forEach(item ->  itemRepository.save(item));
        } catch (JiBXException e) {
            e.printStackTrace();
        } finally {
            is.close();
        }

    }
}