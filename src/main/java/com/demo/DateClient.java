package com.demo;

import date.wsdl.GetDateRequest;
import date.wsdl.GetDateResponse;
import org.springframework.ws.client.core.support.WebServiceGatewaySupport;
import org.springframework.ws.soap.client.core.SoapActionCallback;

public class DateClient extends WebServiceGatewaySupport {
    public GetDateResponse getDate() {
        GetDateResponse response = (GetDateResponse) getWebServiceTemplate()
                .marshalSendAndReceive("http://localhost:8090/ws", new GetDateRequest(),
                        new SoapActionCallback("http://example.com/demo/date-ws/GetDateRequest"));
        return response;
    }
}
