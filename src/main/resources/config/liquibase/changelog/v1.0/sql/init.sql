

CREATE TABLE items
(
  serial_num  varchar primary key,
  title VARCHAR,
  item_description VARCHAR,
  produce_date TIMESTAMP
);

CREATE TABLE orders
(
  id  bigint auto_increment primary key,
  name VARCHAR,
  address VARCHAR,
  amount DOUBLE,
  order_date DATE
);

CREATE TABLE order_items
(
  id  int not null ,
  serial_num varchar,
  amount int,
  order_id bigint not null
);
ALTER TABLE order_items ADD PRIMARY KEY (id, order_id);
